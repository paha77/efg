<?php

/**
 * Namespace
 */
namespace Efg;

if (!defined( 'TL_ROOT' )) die( 'You cannot access this file directly!' );

class FormdataHook extends \Backend {

    public function processEfgFormData($arrToSave, $arrFiles, $intOldId, $arrForm, $arrLabels) {
	
        global $objPage;
        
        $efgID = $arrForm['id'];
        $formId = $arrToSave['formID'];

        $doImport = 0;
        $importAdress = '';

        $salutationMapping = array('HR' => 'mr', 'FR' => 'fr', 'FI' => 'fi', 'FA' => 'fa');

        if($efgID) {
            $objForm = $this->Database->execute("SELECT * FROM tl_form WHERE id = {$efgID}");
            $doImport = $objForm->raImport;
            $importAdress = $objForm->raImportAdress;

            $hasPathfinder = $objForm->hasPathfinder;
            $pathfinderUserID = $objForm->pathfinderUserID;
        }

        if($doImport && !$importAdress) {

            /*
             * This is an emergency routine. If import is checked, but no address is given, send
             * an notification email to us.
             */

            $this->import('Environment');

            $email = new \Email();
            $email->subject = "Fehlende RA Import Konfiguration";
            $email->from = "{$this->Environment->host}@ncm.at";
            $email->fromName = "Contao {$this->Environment->host}";
            $email->text = 'Der RA Import im Formular: ' . $this->Environment->url . '/'.$this->Environment->request . ' ist nicht richtig konfiguriert!!!';
            $email->sendTo('contao@ncm.at');

        }

        if($doImport && $importAdress) {

            #SETTINGS
            $email = new \Email();
            $email->subject = "AnfragenImport";
            $email->from = "contao-import@ncm.at";
            $email->fromName = "Contao Import";

            $mailText = array();

            if($arrToSave['package_id']) {
                $mailText[] = '<form_type>package</form_type>';
            } else {
                $mailText[] = '<form_type>anfrage</form_type>';
            }

            $mailText[] = '<source>anfrage</source>';
            $mailText[] = '<von>' . $arrToSave['cal_von'] . '</von>';
            $mailText[] = '<bis>' . $arrToSave['cal_bis'] . '</bis>';
            $mailText[] = '<anzerw>' . $arrToSave['countAdults'] . '</anzerw>';
            $mailText[] = '<anzkin>' . $arrToSave['countChildren'] . '</anzkin>';

            if($arrToSave['countChildren']) {
                for($i=1;$i<=$arrToSave['countChildren'];$i++) {
                    $mailText[] = '<age' . $i . '>' . $arrToSave['ageChild_'.$i] . '</age' . $i . '>';
                }
            }

            if($arrToSave['room_name']){
                $arrToSave['others'] .= ' - Anfrage für folgendes Zimmer: ' . $arrToSave['room_name'];
            }

            $mailText[] = '<anrede>' . $arrToSave['salutation'] . '</anrede>';
            $mailText[] = '<vorname>' . $arrToSave['firstname'] . '</vorname>';
            $mailText[] = '<name>' . $arrToSave['lastname'] . '</name>';
            $mailText[] = '<email>' . $arrToSave['email'] . '</email>';
            $mailText[] = '<tel>' . $arrToSave['phone'] . '</tel>';
            $mailText[] = '<fax>' . $arrToSave['fax'] . '</fax>';
            $mailText[] = '<strasse>' . $arrToSave['street'] . '</strasse>';
            $mailText[] = '<plz>' . $arrToSave['zip'] . '</plz>';
            $mailText[] = '<ort>' . $arrToSave['city'] . '</ort>';
            $mailText[] = '<land>' . $arrToSave['country'] . '</land>';
            $mailText[] = '<sonstiges>' . $arrToSave['others'] . '</sonstiges>';
            $mailText[] = '<language>' . $GLOBALS['TL_LANGUAGE'] . '</language>';

            if($arrToSave['package_name']) {
                $mailText[] = '<addon_name>Pauschale</addon_name>';
                $mailText[] = '<addon_value>' . $arrToSave['package_name'] . '</addon_value>';
            } elseif ($arrToSave['package_id']) {
              $arrPackageID = explode(",", $arrToSave['package_id']);
                $packageText = array();
                for( $i = 0 ; $i < sizeof($arrPackageID) ; $i++){
                 $packageName = $this->Database->execute("SELECT * FROM tl_ncm_package WHERE id = $arrPackageID[$i] ")->title;
                 $packageName = unserialize($packageName);
                 if ($packageName['de']) {
                    $packageText[] = $packageName['de'];
                 }
                }
                if (count($packageText)) {
                    $mailText[] = '<addon_name>Pauschale</addon_name>';
                    $mailText[] = '<addon_value>' . implode(',',$packageText)  . '</addon_value>';
                }
            }

            if($arrToSave['room_id']) {
              $arrRoomID = explode(",", $arrToSave[room_id]);
                for( $i = 0 ; $i < sizeof($arrRoomID) ; $i++){
                 $roomToken = $this->Database->execute("SELECT * FROM tl_ncm_room WHERE id = $arrRoomID[$i] ")->room_token;
                 #echo $roomToken;
                 $mailText[] = "<anz_" . $roomToken . ">" . "1" . "</anz_" . $roomToken . ">";
                }
            }

            #The formID is ALWAYS sent if imported to the RA.
            $mailText[] = '<PFADFINDER_ENTRY_ID>' . $formId . '</PFADFINDER_ENTRY_ID>';

            if($hasPathfinder && $pathfinderUserID) {
                #If the customer also has Pfadfinder the userID is supplied
                $mailText[] = '<PFADFINDER_USERID>' . $pathfinderUserID . '</PFADFINDER_USERID>';
            }

            $strMailText = implode("\n", $mailText);
            $email->text = $strMailText;
            $email->sendTo($importAdress);

        } elseif($hasPathfinder) {
            #Only Pathfinder, no RA import.

            $this->Import('Environment');

            #SETTINGS
            $pfemail = new \Email();
            $pfemail->subject = $objForm->formattedMailSubject;

            // default sender
            $pfemail->from = "contao@ncm.at";
            $pfemail->fromName = "Contao Master 2.10";

            // try to send it from the guest's credentials
            if(strlen($arrToSave['email'])){
                $pfemail->from = $arrToSave['email'];
            } else {
                $pfemail->from = "contao@ncm.at";
            }
            //lastname

            if(strlen($arrToSave['lastname'])){
                $pfemail->fromName = $arrToSave['lastname'] . ' ' . $arrToSave['firstname'];
            } else {
                $pfemail->fromName = "Checkeffect Pfadfinder";
            }

            $pfmailto = $formId . '.' . $pathfinderUserID . '@thinkthewebway.com';

            $mailText = array();
            $strVonTag = $GLOBALS['TL_LANG']['msc']['weekdays_short'][date('w', strtotime($arrToSave['cal_von']))];
            $strBisTag = $GLOBALS['TL_LANG']['msc']['weekdays_short'][date('w', strtotime($arrToSave['cal_bis']))];

            $mailText[] = "Anreise: {$strVonTag}, {$arrToSave['cal_von']}";
            $mailText[] = "Abreise: {$strBisTag}, {$arrToSave['cal_bis']}";
            $mailText[] = "Erwachsene: {$arrToSave['countAdults']}";
            $mailText[] = "Kinder: {$arrToSave['countChildren']}";

            $childAges = array();
            if($arrToSave['countChildren']) {
                for($i=1;$i<=$arrToSave['countChildren'];$i++) {
                    $childAges[] = $arrToSave['ageChild_'.$i];
                }
            }
            $strChildAges = implode(',', $childAges);
            $mailText[] = "Alter der Kinder: {$strChildAges}";
            $mailText[] = "Anrede: {$arrToSave['salutation']}";
            $mailText[] = "Nachname: {$arrToSave['lastname']}";
            $mailText[] = "Vorname: {$arrToSave['firstname']}";
            $mailText[] = "Email: {$arrToSave['email']}";
            $mailText[] = "Telefon: {$arrToSave['phone']}";
            $mailText[] = "Straße: {$arrToSave['street']}";
            $mailText[] = "PLZ: {$arrToSave['zip']}";
            $mailText[] = "Ort: {$arrToSave['city']}";
            $mailText[] = "Land: {$arrToSave['country']}";
            $mailText[] = "Sonstiges: {$arrToSave['others']}";
            $mailText[] = "Sprache: {$GLOBALS['TL_LANGUAGE']}";
            $mailText[] = "\n";
            $mailText[] = "CHECKEFFECTINFO_TXT";
            #$mailText[] = $arrToSave['formID'] . $pathfinderUserID . '@thinkthewebway.com';
            $strMailText = implode("\n", $mailText);

            $pfemail->text = $strMailText;
            $pfemail->sendTo($pfmailto);

        }
    }
    
    public function processDynamicData($arrSubmitted, &$arrForm=false, $arrFiles=false, $arrLabels=false) {
        global $objPage;

        $efgID = $arrForm['id'];

        // handle email and redirect settings
        // get ContentElement holding the form
        $objFormElement = $this->Database->prepare("SELECT
                                                    c.*
                                                    FROM
                                                    tl_content c,
                                                    tl_page p,
                                                    tl_article a
                                                    WHERE  c.form=?
                                                         AND c.pid = a.id
                                                         AND a.pid = p.id
                                                         AND p.id = ? ")
                            ->limit(1)
                            ->execute($efgID, $objPage->id);
        /**
         * Return if we didn't found a ContentElement
         */
        if ($objFormElement->numRows<1) {
                return;
        } else {
            // convert the row object to an array
            $contentElementArr = $objFormElement->fetchAssoc();

            // if there is set the ncm_jumpTo page we use this to forward
            if($contentElementArr['efg_jumpTo'] > 0){
                $arrForm['jumpTo'] = (int)$contentElementArr['efg_jumpTo'];
            }

            // if the sendFormattedMail checkbox is set for this form
            if($arrForm['sendFormattedMail']){

                // if the ncm_dynamic_email is set in this ContentElement
                if(strlen($contentElementArr['efg_dynamic_email']) > 0){

                    // get the new email
                    $new_email = $contentElementArr['efg_dynamic_email'];

                    // a simple regular expression for validating the email again (first time in dca)
                    if($this->isValidEmailAddress($new_email)){
                        /**
                         * If 'ncm_only_dynamic_email' was checked in the ContentElement,
                         * we send the mail only to this email address otherwise we additional
                         * send the mail to $new_mail (simply , seperated)
                         **/
                        if($contentElementArr['efg_only_dynamic_email']){
                            $arrForm['formattedMailRecipient'] = $new_email;
                        } else {
                            $arrForm['formattedMailRecipient'] .= ','.$new_email;
                        }
                    }
                }
            }
        }
        
        
    }
    
    
}

