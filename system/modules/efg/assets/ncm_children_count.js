// a simple jQuery plugin
// @see: http://learn.jquery.com/plugins/basic-plugin-creation/
(function($) {
    jQuery.fn.countChildren = function() {
        return this.each(function() {
            // check if we have the countChildren field
            if (!jQuery(this).find("[name=countChildren]")
                    .change(function() {
                        jQuery(this).setChildNumDropdowns();
                    })
                    .size()) {
                return;
            }
            // call it immediately
            jQuery(this).setChildNumDropdowns();
        });
    }

    // shows as many dropdowns as many children are set    
    jQuery.fn.setChildNumDropdowns = function() {
        // hide all sibling elements' containing divs
        jQuery(this).closest("form").find(".ageChildren").closest(".form-group").hide();
        // display as many as the value is
        for (var index=1; index <= jQuery(this).val(); index++) {
            jQuery(this).closest("form").find("[name=ageChild_"+ index +"]").closest(".form-group").show();
        }
    }
})(jQuery);

// checks the form for "number of children" elements and age fields
jQuery(function() {
    jQuery("form").countChildren();    
});




