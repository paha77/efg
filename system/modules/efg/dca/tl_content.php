<?php

/**
 * Add a palette to tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['palettes']['form'] .= ';{efg_dynamic_legend:hide},efg_dynamic_email,efg_only_dynamic_email,efg_jumpTo;';


//@TODO:
//$GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'][] = array('tl_content_ncm', 'checkPermission');

/**
 * Add fields to tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['fields']['efg_dynamic_email'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['efg_dynamic_email'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'rgxp' => 'email'),
    'sql'                     => "varchar(255) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_content']['fields']['efg_only_dynamic_email'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['efg_only_dynamic_email'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('mandatory'=>false),
    'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['efg_jumpTo'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['efg_jumpTo'],
    'exclude'                 => true,
    'inputType'               => 'pageTree',
    'eval'                    => array('mandatory'=>false, 'fieldType' => 'radio'),
    'sql'                     => "int(10) unsigned NOT NULL default '0'",
);

// @TODO
/**
 * Class tl_formdata
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @copyright  Thomas Kuhn 2007 - 2011
 * @author     Thomas Kuhn <mail@th-kuhn.de>
 * @package    efg
 */
//class tl_content_ncm extends Backend
//{
//    /**
//    * Check permissions to edit table tl_formdata
//    */
//    public function checkPermission()
//    {
//        $this->import('BackendUser', 'User');
//        $arrFields = array_keys($GLOBALS['TL_DCA']['tl_content']['fields']);
//
//        // check/set restrictions
//        foreach ($arrFields as $strField)
//        {
//            if ($GLOBALS['TL_DCA']['tl_content']['fields'][$strField]['exclude'] == true)
//            {
//                if ($this->User->isAdmin || $this->User->hasAccess('tl_content::'.$strField, 'alexf') == true)
//                {
//                    $GLOBALS['TL_DCA']['tl_content']['fields'][$strField]['exclude'] = false;
//                }
//            }
//        }
//    }   
//}
