<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   Efg
 * @author    Thomas Kuhn <mail@th-kuhn.de>
 * @license   http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 * @copyright Thomas Kuhn 2007-2014
 */


// This file is created when saving a form in form generator
// last created on 2014-03-31 11:33:04


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['formdata'] = 'Form data';
$GLOBALS['TL_LANG']['MOD']['efg'] = 'Form data';
$GLOBALS['TL_LANG']['MOD']['feedback'] = array('All results', 'Stored data from forms.');
$GLOBALS['TL_LANG']['MOD']['fd_anfrage_de'] = array('Anfrage DE', 'Stored data from form "Anfrage DE".');
$GLOBALS['TL_LANG']['MOD']['fd_anfrage-en'] = array('Anfrage EN', 'Stored data from form "Anfrage EN".');
$GLOBALS['TL_LANG']['MOD']['fd_kontakt-de'] = array('Kontakt DE', 'Stored data from form "Kontakt DE".');
$GLOBALS['TL_LANG']['MOD']['fd_kontakt-en'] = array('Kontakt EN', 'Stored data from form "Kontakt EN".');
$GLOBALS['TL_LANG']['MOD']['fd_gutschein-de'] = array('Gutschein DE', 'Stored data from form "Gutschein DE".');
$GLOBALS['TL_LANG']['MOD']['fd_gutschein-en'] = array('Gutschein EN', 'Stored data from form "Gutschein EN".');
$GLOBALS['TL_LANG']['MOD']['fd_prospekt-de'] = array('Prospekt DE', 'Stored data from form "Prospekt DE".');
$GLOBALS['TL_LANG']['MOD']['fd_prospekt-en'] = array('Prospekt EN', 'Stored data from form "Prospekt EN".');
$GLOBALS['TL_LANG']['MOD']['fd_rueckruf-de'] = array('Rückruf DE', 'Stored data from form "Rückruf DE".');
$GLOBALS['TL_LANG']['MOD']['fd_rueckruf-en'] = array('Rückruf EN', 'Stored data from form "Rückruf EN".');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['formdatalisting'] = array('Listing form data', 'Use this module to list the records of a certain form data table in the front end.');
