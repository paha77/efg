<?php

$GLOBALS['TL_LANG']['tl_content']['efg_dynamic_legend'] = 'Dynamische Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['efg_dynamic_email'] = array('Dynamische E-Mail','E-Mail an die Sie die Mail versenden wollen (Adminmail)');
$GLOBALS['TL_LANG']['tl_content']['efg_only_dynamic_email'] = array('Nur "Dynamische E-Mail" verwenden','Wenn Sie die E-Mail nur an die "Dynamische E-Mail" versenden wollen (standardmäßig wird zusätzlich an diese versendet)');
$GLOBALS['TL_LANG']['tl_content']['efg_jumpTo'] = array('Dynamische Weiterleitungsseite','Wenn Sie hier eine andere Weiterleitungsseite verwenden wollen, als Sie im Formulargenerator gewählt haben.');
 
