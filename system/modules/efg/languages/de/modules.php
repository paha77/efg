<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   Efg
 * @author    Thomas Kuhn <mail@th-kuhn.de>
 * @license   http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 * @copyright Thomas Kuhn 2007-2014
 */


// This file is created when saving a form in form generator
// last created on 2014-03-31 11:33:04


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['formdata'] = 'Formular-Daten';
$GLOBALS['TL_LANG']['MOD']['efg'] = 'Formular-Daten';
$GLOBALS['TL_LANG']['MOD']['feedback'] = array('Feedback', 'Gespeicherte Daten aus Formularen.');
$GLOBALS['TL_LANG']['MOD']['fd_anfrage_de'] = array('Anfrage DE', 'Gespeicherte Daten aus Formular "Anfrage DE".');
$GLOBALS['TL_LANG']['MOD']['fd_anfrage-en'] = array('Anfrage EN', 'Gespeicherte Daten aus Formular "Anfrage EN".');
$GLOBALS['TL_LANG']['MOD']['fd_kontakt-de'] = array('Kontakt DE', 'Gespeicherte Daten aus Formular "Kontakt DE".');
$GLOBALS['TL_LANG']['MOD']['fd_kontakt-en'] = array('Kontakt EN', 'Gespeicherte Daten aus Formular "Kontakt EN".');
$GLOBALS['TL_LANG']['MOD']['fd_gutschein-de'] = array('Gutschein DE', 'Gespeicherte Daten aus Formular "Gutschein DE".');
$GLOBALS['TL_LANG']['MOD']['fd_gutschein-en'] = array('Gutschein EN', 'Gespeicherte Daten aus Formular "Gutschein EN".');
$GLOBALS['TL_LANG']['MOD']['fd_prospekt-de'] = array('Prospekt DE', 'Gespeicherte Daten aus Formular "Prospekt DE".');
$GLOBALS['TL_LANG']['MOD']['fd_prospekt-en'] = array('Prospekt EN', 'Gespeicherte Daten aus Formular "Prospekt EN".');
$GLOBALS['TL_LANG']['MOD']['fd_rueckruf-de'] = array('Rückruf DE', 'Gespeicherte Daten aus Formular "Rückruf DE".');
$GLOBALS['TL_LANG']['MOD']['fd_rueckruf-en'] = array('Rückruf EN', 'Gespeicherte Daten aus Formular "Rückruf EN".');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['formdatalisting'] = array('Auflistung Formular-Daten', 'Verwenden Sie dieses Modul dazu, die Daten einer beliebigen Formular-Daten-Tabelle im Frontend aufzulisten.');
